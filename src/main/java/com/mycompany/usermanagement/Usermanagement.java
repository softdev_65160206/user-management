/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.usermanagement;

import java.util.ArrayList;

/**
 *
 * @author aof2a
 */
public class Usermanagement {

    public static void main(String[] args) {
        User admin = new User("admin","Administrator","pass@1234",'M','A');
        User user1 = new User("user1","User 1","pass@1234",'M','U');
        User user2 = new User("user2","User 2","pass@1234",'F','U');
        User user3 = new User("user3","User 3","pass@1234",'F','U');

        System.out.println(admin);
        System.out.println(user1);
        System.out.println(user2);
        User[] UserArr = new User[4];
        UserArr[0] = admin;
        UserArr[1] = user1;
        UserArr[2] = user2;
        UserArr[3] = user3;
        System.out.println("Print for userArr");
        for(int i = 0; i < UserArr.length;i++){
            System.out.println(UserArr[i]);
        }
        ArrayList<User> userList = new ArrayList<User>();
        userList.add(admin);
        System.out.println(userList.get(userList.size()-1)+"List size =" + userList.size());
        userList.add(user1);
        System.out.println(userList.get(userList.size()-1)+"List size =" + userList.size());
        userList.add(user2);
        System.out.println(userList.get(userList.size()-1)+"List size =" + userList.size());
        userList.add(user3);
        System.out.println(userList.get(userList.size()-1)+"List size =" + userList.size());
        for(User u: userList){
            System.out.println(u);
        }
        
        
    }
}
